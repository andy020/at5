//
//  ADVRoundProgressView.m
//  RTVNH
//
//  Created by Andy Pijpers on 31-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import "timeProgressView.h"
#import "CERoundProgressView.h"

@interface timeProgressView ()

@property (nonatomic, strong) CERoundProgressView   *pieView;
@property (nonatomic, strong) UIImageView           *imgView;

- (void)_initIVars;

@end

@implementation timeProgressView

@synthesize pieView;
@synthesize piePadding;
@synthesize imgView;
@synthesize progress;
@synthesize image;
@synthesize tintColor;


#pragma mark - View lifecycle

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _initIVars];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self _initIVars];
    }
    return self;
}

- (void)_initIVars {
    self.backgroundColor = [UIColor clearColor];
    
    self.pieView = [[CERoundProgressView alloc] initWithFrame:self.bounds];
    self.pieView.tintColor = self.tintColor;
    self.pieView.startAngle = M_PI/2;
    [self addSubview:self.pieView];
    
    self.imgView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imgView.image = [UIImage imageNamed:@"progress-circle-large"];
    [self addSubview:self.imgView];
    
    self.piePadding = 1.5;
}


#pragma mark - Accessors

- (void)setProgress:(float)newProgress {
    if (newProgress < 0) {
        newProgress = 0.0;
    } else if(newProgress > 1.0) {
        newProgress = 1.0;
    }
    
    if (progress == newProgress) {
        return;
    }
    
    progress = newProgress;
    self.pieView.progress = progress;

}

- (void)setImage:(UIImage *)newImage {
    if ([image isEqual:newImage]) {
        return;
    }
    
    image = newImage;
    self.imgView.image = image;
}

- (void)setTintColor:(UIColor *)newTintColor {
    if ([tintColor isEqual:newTintColor]) {
        return;
    }
    
    tintColor = newTintColor;
    self.pieView.tintColor = tintColor;
}

- (void)setPiePadding:(float)newPiePadding {
    if (piePadding == newPiePadding) {
        return;
    }
    
    piePadding = newPiePadding;
    CGRect pieFrame = self.bounds;
    pieFrame.origin.x = piePadding;
    pieFrame.origin.y = piePadding;
    pieFrame.size.width -= 2*pieFrame.origin.x;
    pieFrame.size.height -= 2*pieFrame.origin.y;
    
    self.pieView.frame = pieFrame;
}


@end
