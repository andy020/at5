//
//  ADVRoundProgressView.h
//  RTVNH
//
//  Created by Andy Pijpers on 31-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface timeProgressView : UIView

@property (nonatomic)           float    progress;
@property (nonatomic)           float    piePadding;
@property (nonatomic, strong)   UIImage *image;
@property (nonatomic, retain) UIColor *tintColor UI_APPEARANCE_SELECTOR;

@end
