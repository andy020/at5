//
//  Share.h
//  Kwisthet
//
//  Created by student on 30-06-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAAShare : UIViewController{
    IBOutlet UIImageView *background;
    IBOutlet UIButton *facebookShare;
    IBOutlet UIButton *twitterShare;
    IBOutlet UIImageView *popupBg;
}

@property (strong, nonatomic) IBOutlet UIButton *dismisclick;

- (IBAction)dismicclick:(id)sender;

- (IBAction)postToTwitter:(id)sender;

- (IBAction)postToFacebook:(id)sender;


@end
