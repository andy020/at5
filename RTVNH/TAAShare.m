//
//  Share.m
//  Kwisthet
//
//  Created by student on 30-06-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import "TAAShare.h"
#import "TAAVraag.h"
#define facebookView 1
#define twitterView 2
#import "TAAStore.h"
#import <Social/Social.h>
#import "JMImageCache.h"
#import "TAAUitslagViewController.h"

@interface TAAShare ()

@end

@implementation TAAShare
int long highScore;

- (IBAction)postToFacebook:(id)sender {
     NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    if ([appType isEqualToString:@"RTVNH"]) {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];

        
        highScore =[[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
        
        [controller setInitialText:[NSString stringWithFormat:@"Hoppa, %li punten! Denk jij meer te weten van het nieuws uit Noord-Holland? \n Speel de game via http://www.rtvnh.nl/Kwisthet \n #Kwisthet! #RTVNH",highScore]];
        [controller addImage:[UIImage imageNamed:@"screenshotimage.png"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oeps"
                                                        message:[NSString stringWithFormat:@"Je hebt geen Facebook geinstalleerd of je instellingen staan het niet toe."]
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:@"Download", nil];
        alert.tag = facebookView;
        [alert show];
    }
        
    } else if  ([appType isEqualToString:@"AT5"]) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
           highScore =[[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
            
            
            [controller setInitialText:[NSString stringWithFormat:@"Hoppa, %ld punten! Denk jij meer te weten van het nieuws uit Amsterdam? \n Speel de game via http://www.at5.nl/Kwisthet \n #Kwisthet! #AT5",highScore]];
            [controller addImage:[UIImage imageNamed:@"screenshotimage.png"]];
            
            [self presentViewController:controller animated:YES completion:Nil];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oeps"
                                                            message:[NSString stringWithFormat:@"Je hebt geen Facebook geïnstalleerd of je moet nog eens inloggen onder Instellingen --> Facebook!"]
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:@"Download", nil];
            alert.tag = facebookView;
            [alert show];
    }
    } else {
        NSLog(@"it is an unknown app");

    }
    
}

- (IBAction)postToTwitter:(id)sender {
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
       highScore =[[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
        [tweetSheet setInitialText:[NSString stringWithFormat:@"Hoppa, %li punten! Denk jij meer te weten van het nieuws uit Noord-Holland? \n Speel de game via http://www.rtvnh.nl/Kwisthet \n #Kwisthet! #RTVNH",highScore]];
               [self presentViewController:tweetSheet animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oeps"
                                                        message:[NSString stringWithFormat:@"Je hebt geen Twitter geinstalleerd of je instellingen staan het niet toe."]
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:@"Download", nil];
        alert.tag = twitterView;
        [alert show];
    }
    } else if  ([appType isEqualToString:@"AT5"]) {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
        highScore =[[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
            
            [tweetSheet setInitialText:[NSString stringWithFormat:@"Hoppa, %li punten! Denk jij meer te weten van het nieuws uit Amsterdam? \n Speel de game via http://www.at5.nl/Kwisthet \n #Kwisthet! #AT5",highScore]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oeps"
                                                            message:[NSString stringWithFormat:@"Je hebt geen Twitter geinstalleerd of je instellingen staan het niet toe."]
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:@"Download", nil];
            alert.tag = twitterView;
            [alert show];
        }
    } else {
        NSLog(@"it is an unknown app");
    }

}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == facebookView && buttonIndex == 1) {
        [[UIApplication sharedApplication]
         openURL:[NSURL URLWithString:@"https://itunes.apple.com/nl/app/facebook/id284882215?mt=8&uo=4"]];
    } else if (alertView.tag == twitterView && buttonIndex == 1) {
        [[UIApplication sharedApplication]
         openURL:[NSURL URLWithString:@"https://itunes.apple.com/nl/app/twitter/id333903271?mt=8&uo=4"]];
    }
}

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
        NSString* alertViewShare = @"http://www.rtvnh.nl/static/img/kwisthet/behind_alert_view.png";
        [background setImageWithURL:[NSURL URLWithString:alertViewShare]
                        placeholder:[UIImage imageNamed:@"alert_view.png"]];
        NSString* popupBG = @"http://www.rtvnh.nl/static/img/kwisthet/popupBackground.png";
        [popupBg setImageWithURL:[NSURL URLWithString:popupBG]
                        placeholder:[UIImage imageNamed:@"popupBackground.png"]];
        NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/twitter_button.png"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *NH_twitter = [UIImage imageWithData:data];
        [twitterShare setImage:NH_twitter forState:UIControlStateNormal];
        NSURL *url2 = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/facebook_button.png"];
        NSData *data2 = [NSData dataWithContentsOfURL:url2];
        UIImage *NH_facebook = [UIImage imageWithData:data2];
        [facebookShare setImage:NH_facebook forState:UIControlStateNormal];
    } else if  ([appType isEqualToString:@"AT5"]) {
        NSString* alertViewShare = @"http://www.rtvnh.nl/static/img/kwisthet/behind_alert_view.png";
        [background setImageWithURL:[NSURL URLWithString:alertViewShare]
                        placeholder:[UIImage imageNamed:@"alert_view.png"]];
        NSString* popupBG = @"http://www.rtvnh.nl/static/img/kwisthet/AT5_popupBackground.png";
        [popupBg setImageWithURL:[NSURL URLWithString:popupBG]
                     placeholder:[UIImage imageNamed:@"popupBackground.png"]];
        NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/twitter_button.png"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *NH_twitter = [UIImage imageWithData:data];
        [twitterShare setImage:NH_twitter forState:UIControlStateNormal];
        NSURL *url2 = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/facebook_button.png"];
        NSData *data2 = [NSData dataWithContentsOfURL:url2];
        UIImage *NH_facebook = [UIImage imageWithData:data2];
        [facebookShare setImage:NH_facebook forState:UIControlStateNormal];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismicclick:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
