//
//  TAAStore.m
//  RTVNH
//
//  Created by student on 23-05-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import "TAAStore.h"
#import "TAAVraag.h"
#import "TAAScramble.h"
#import "XMLData.h"

@interface TAAStore ()
{
NSXMLParser *parser;
NSXMLParser *bannedWordsParser;
NSString *word;
NSMutableDictionary *item;
NSMutableString *title;
NSString *tag;
}


@end

@implementation TAAStore



+ (instancetype)quizStore
{
    static TAAStore *quizStore;
    
    if (!quizStore) {
        quizStore = [[self alloc] initPrivate];
    }
    
    return quizStore;
}


- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[TAAStore quizStore]"
                                 userInfo:nil];
    return nil;
}


- (instancetype)initPrivate
{
    self = [super init];
    
    if (self) {
        _privateVragen = [[NSMutableArray alloc] init];
        _gevraagdeVragen = [[NSMutableArray alloc] init];
        _feeds = [[NSMutableArray alloc] init];
        _bannedwords = [[NSMutableArray alloc] init];

        
        NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
        
        if ([appType isEqualToString:@"RTVNH"]) {

            NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/feed/nieuws"];
            parser = [[NSXMLParser alloc] initWithContentsOfURL:url];


        } else if  ([appType isEqualToString:@"AT5"]) {
            NSURL *url = [NSURL URLWithString:@"http://at5.nl/feed"];
            parser = [[NSXMLParser alloc] initWithContentsOfURL:url];

        } else {
            NSLog(@"error");
        }

        
        
        [parser setDelegate:self];
        [parser setShouldResolveExternalEntities:NO];
        [parser parse];
        
        NSLog(@"parser error: %@",[parser parserError]);
        
        XMLData *data = [[XMLData alloc] init];
        if ([appType isEqualToString:@"RTVNH"]) {
            NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/banwords.xml"];
            [data parseDocumentWithURL:url];
            NSLog(@"Woorden RTVNH: %@", data.myArray);
        } else if  ([appType isEqualToString:@"AT5"]) {
            NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_banwords.xml"];
            [data parseDocumentWithURL:url];
            NSLog(@"Woorden AT5: %@", data.myArray);
        }
        
        //               NSLog(@"array: %@", data.myArray);
        
        NSArray *badWords = data.myArray;
        
        if ([self.feeds count]>0) {
            NSLog (@"dit zijn de aantal feeds: %@", self.feeds);
            
           for (NSDictionary *myItem in self.feeds) {
               TAAVraag *vraag = [[TAAVraag alloc] init];

  
//               NSLog(@"Verbannen Woorden: %@", badWords);

               NSString *deZin = [myItem objectForKey:@"title"];
               for (NSString* string in badWords) {
                   deZin = [[deZin stringByReplacingOccurrencesOfString:string withString:@"" ] mutableCopy];
               }
               

               NSCharacterSet *notAllowedChars2 = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -:1234567890%#"] invertedSet];
               NSString *krantKop = [[deZin componentsSeparatedByCharactersInSet:notAllowedChars2] componentsJoinedByString:@""];
               NSCharacterSet *nonalphanumericSet = [[ NSCharacterSet alphanumericCharacterSet ] invertedSet ];
               

               //vraag.krantenKop = deZin;
               vraag.krantenKop = krantKop;

               NSString *langsteWoord = [self langsteWoordinZin:krantKop];

               NSString *besteWoord = [self besteWoordinZin:krantKop];
               
               if ([besteWoord  isEqual: @""]) {
                   //
                   vraag.randomWoord = [langsteWoord stringByTrimmingCharactersInSet:nonalphanumericSet];

               } else {
                   vraag.randomWoord = [besteWoord stringByTrimmingCharactersInSet:nonalphanumericSet];


               }
               
                [self.privateVragen addObject:vraag];
            }
        }
    }
    return self;
}



- (NSString *) langsteWoordinZin:(NSString *) kop
{
    NSArray *woorden = [kop componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSString *langsteWoord = @"";
    NSUInteger langsteAantal = 0;
    
    for (int i=0 ; i<[woorden count]; i++) {
        
        
        if ([woorden[i] length] > langsteAantal) {
            langsteAantal = [woorden[i] length];
            langsteWoord = woorden[i];
        }
    }

    
    //check op non-alfabet letters en sloop ze eruit.
    
    return langsteWoord;
}


- (NSString *) besteWoordinZin:(NSString *) kop
{
    NSArray *woorden = [kop componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *besteWoord = @"";
    //NSUInteger langsteAantal = 0;
    
    for (int i=0 ; i<[woorden count]; i++) {
        
        int long woordLengte = [woorden[i] length];
        
        if (woordLengte >= 6 && woordLengte <= 10) {
                        besteWoord = woorden[i];
        }
        
        
    }
    
    return besteWoord;
}

- (void)oudeVragen
{
    for (int i=0; i<2; i++) {
        TAAVraag *vraag = [[TAAVraag alloc] init];
        vraag.krantenKop = @"andere zin";
        [self.privateVragen addObject:vraag];
        
        
    }
}

- (NSArray *)allItems
{
   return self.privateVragen;
}

- (void)removeVraag:(TAAVraag *)vraag
{
    [self.privateVragen removeObjectIdenticalTo:vraag];
}


- (NSArray *)alleGevraagdeVragen
{
    return [self.gevraagdeVragen copy];
    
        
}


- (void)voegVraagToeAanGevraagdeVragen:(TAAVraag *)vraag
{
    [self.gevraagdeVragen addObject:vraag];
}


- (void)moveVraagAtIndex:(NSInteger)fromIndex
                toIndex:(NSInteger)toIndex
{
    if (fromIndex == toIndex) {
        return;
    }
    TAAVraag *vraag = self.privateVragen[fromIndex];
    
    [self.privateVragen removeObjectAtIndex:fromIndex];
    
    [self.privateVragen insertObject:vraag atIndex:toIndex];
}

#pragma mark - XML parser stuff
// dit wordt aangeroepen bij het BEGIN van een XML element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {

    tag = elementName;
    
    if ([tag isEqualToString:@"item"]) {
        item    = [[NSMutableDictionary alloc] init];
    }
    if ([tag isEqualToString:@"title"]) {
        title   = [[NSMutableString alloc] init];


    }
}

// dit wordt aangeroepen bij het EINDE van een XML element
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        if (item) {
            [_feeds addObject:[item copy]];             // sla de vraag op in je _feeds array
            item = nil;
        }
    }
    if ([elementName isEqualToString:@"title"]) {
        if (title) {
            // optioneel: test of de titel GESCHIKT is
            [item setObject:title forKey:@"title"];
            title = nil;
        }
    }

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {

    if ([tag isEqualToString:@"title"]) {
        [title appendString:string];

    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    

}

// dit wordt aangeroepen bij het EINDE van een XML element
//- (void)bannedWordsParser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
//    
//    NSLog(@"kom ik in de parse method 1?");
//    
//    if ([tag isEqualToString:@"words"]) {
//        if (word) {
//            [_bannedwords addObject:[word copy]];             // sla de vraag op in je _feeds array
//            word = nil;
//            NSLog(@"dit zijn de banWoorden: %@", word);
//        }
//    }
//    if ([elementName isEqualToString:@"title"]) {
//        if (title) {
//            // optioneel: test of de titel GESCHIKT is
//            [item setObject:title forKey:@"title"];
//            title = nil;
//        }
//    }
    
//}

- (void)bannedWordsParser:(NSXMLParser *)parser foundCharacters:(NSString *)string {

    if ([tag isEqualToString:@"title"]) {
        [title appendString:string];
        
    }
}

- (void)bannedWordsParserDidEndDocument:(NSXMLParser *)parser {
    
    
}



@end