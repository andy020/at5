//
//  TAAStore.h
//  RTVNH
//
//  Created by student on 23-05-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLData.h"

@class TAAVraag;

@interface TAAStore : NSObject <NSXMLParserDelegate>

@property (nonatomic, readonly) NSArray *allItems;
@property (nonatomic, readonly) NSArray *alleGevraagdeVragen;
@property (nonatomic) NSMutableArray *privateVragen;        // bevat ALLE vragen die uit de feed komen
@property (nonatomic) NSMutableArray *gevraagdeVragen;
@property (nonatomic) NSMutableArray *gehusseldWoord;
@property (nonatomic) NSMutableArray *feeds;                // dit bevat de json feed met vragen van RTVNH
@property (nonatomic) NSMutableArray *bannedwords;
@property (nonatomic) XMLData *myArray;
@property (copy, nonatomic) NSURL *url;
@property (nonatomic) BOOL *isAlphaNumeric;
+ (instancetype)quizStore;

//- (TAAVraag *)createVraag;
- (void)removeVraag:(TAAVraag *)vraag;


- (void)voegVraagToeAanGevraagdeVragen:(TAAVraag *)vraag;


- (void)moveVraagAtIndex:(NSInteger)fromIndex
                toIndex:(NSInteger)toIndex;

@end
