//
//  XMLData.m
//  XMLParse
//
//  Created by Stijn Oomes on 8/7/14.
//  Copyright (c) 2014 Stijn Oomes. All rights reserved.
//

#import "XMLData.h"

@implementation XMLData


-(BOOL)parseDocumentWithURL:(NSURL *)url {
    if (url == nil)
        return NO;
    
    _myArray = [[NSMutableArray alloc] init];
    
    // this is the parsing machine
    NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    // this class will handle the events
    [xmlparser setDelegate:self];
    [xmlparser setShouldResolveExternalEntities:NO];
    
    // now parse the document
    BOOL ok = [xmlparser parse];
    if (ok == NO)
        NSLog(@"error");
    else
        NSLog(@"OK");
    
    //NSLog(@"array: %@", _myArray);
    
    return ok;
}

-(void)parserDidStartDocument:(NSXMLParser *)parser {
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    tag = elementName;
    
    if (namespaceURI != nil)
    
    if (qName != nil)
        NSLog(@"qualifiedName: %@", qName);
    
    // print all attributes for this element
    NSEnumerator *attribs = [attributeDict keyEnumerator];
    NSString *key, *value;
    
    while((key = [attribs nextObject]) != nil) {
        value = [attributeDict objectForKey:key];
    }
    
    // add code here to load any data members
    // that your custom class might have
    
    if ([elementName isEqualToString:@"word"]) {

        myWord = [[NSMutableString alloc] init];
            }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([tag isEqualToString:@"word"]) {
        
        [myWord appendString:string ];
    }
}


-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if (myWord) {
        
//        NSLog(@"words: %@", _myArray);
        [_myArray addObject:[myWord copy]];
        myWord = nil;
    }
//    
//    NSLog(@"didEndElement: %@", elementName);
}


// error handling
-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"XMLParser error: %@", [parseError localizedDescription]);
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError {
    NSLog(@"XMLParser error: %@", [validationError localizedDescription]);
}

@end