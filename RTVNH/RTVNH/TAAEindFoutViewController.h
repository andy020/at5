//
//  TAAEindFoutViewController.h
//  RTVNH
//
//  Created by Andy Pijpers on 30-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAAVraag.h"

@interface TAAEindFoutViewController : UIViewController{
    IBOutlet UIButton *uitslag;
    IBOutlet UIImageView *background;
    IBOutlet UIImageView *alert_fout;
}
@property (nonatomic, strong) TAAVraag *vraag;
@end
