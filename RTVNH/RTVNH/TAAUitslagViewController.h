//
//  TAAUitslagViewController.h
//  RTVNH
//
//  Created by Andy Pijpers on 31-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "TAAVraagViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface TAAUitslagViewController : UIViewController{
IBOutlet UIButton *share;
IBOutlet UIButton *download;
IBOutlet UIButton *speelopnieuw;
IBOutlet UIImageView *background;
NSInteger quizCount;
    NSInteger weekday;
}
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *highScoreLabel;
@property (nonatomic, strong) TAAVraagViewController *setupQuiz;
@property(nonatomic, readonly) UIWindow *window;



@property (nonatomic, strong)NSString *filePath;
//@property (nonatomic, strong)NSString *highScore;

@end
