//
//  NewScoreVC.m
//  Kwisthet
//
//  Created by student on 27-08-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import "NewScoreVC.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <Parse/Parse.h>
#import "MBProgressHUD.h"
#import "TAAStore.h"
#import "TAAVraag.h"


@interface NewScoreVC ()

- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;



@end

@implementation NewScoreVC
int long highScore;
int score;

NSInteger saveCount;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.Save setEnabled:NO];

    
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    if ([appType isEqualToString:@"RTVNH"]) {
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"background.png"]];
        [self.tableView setBackgroundView:imageView];
    }else if  ([appType isEqualToString:@"AT5"]) {
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"backgroundAT5.png"]];
        [self.tableView setBackgroundView:imageView];
    }
    _nameTextField.delegate = self;
    _emailTextField.delegate = self;
    highScore =[[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
    self.scoreLabel.text = [NSString stringWithFormat:@"%ld punten",highScore];

}


- (IBAction)editingChanged {
    if ([_nameTextField.text length] != 0) {
        [self.Save setEnabled:YES];
           }else {
        [self.Save setEnabled:NO];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
    }
}


- (void)showPhotoLibary
{
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return;
    }
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    // Displays saved pictures from the Camera Roll album.
    mediaUI.mediaTypes = @[(NSString*)kUTTypeImage];
    
    // Hides the controls for moving & scaling pictures
    mediaUI.allowsEditing = NO;
    
    mediaUI.delegate = self;

    
//    [self.navigationController presentModalViewController: mediaUI animated: YES];


}


- (IBAction)save:(id)sender {

           if([sender respondsToSelector:@selector(setHidden:)])
            [sender setHidden:YES];
        
    // Create PFObject with recipe information
    PFObject *score = [PFObject objectWithClassName:@"HighScore"];
    [score setObject:_nameTextField.text forKey:@"naam"];
    [score setObject:_scoreLabel.text forKey:@"score"];
    [score setObject:_emailTextField.text forKey:@"email"];
    
    
    
    // Show progress
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Uploading";
    [hud show:YES];
    
    // Upload recipe to Parse
    [score saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [hud hide:YES];
        
        if (!error) {
            // Show success message
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Score Geupload" message:@"Je score is nu opgeslagen." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

            
            // Notify table view to reload the recipes from Parse cloud
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
            
            
            // Dismiss the controller
//            [self dismissViewControllerAnimated:YES completion:nil];
            [self performSegueWithIdentifier:@"weekScore" sender:self];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Failure" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
    }];
        }



- (IBAction)cancel:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Waarschuwing" message:@"Weet je zeker dat je terug wilt? Je Score word niet opgeslagen" delegate:self cancelButtonTitle:@"Ja" otherButtonTitles:@"Nee", nil];
    [alert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:@"Ja"]) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
}
- (void)viewDidUnload {
    
    [self setNameTextField:nil];
    [self setEmailTextField:nil];
    [super viewDidUnload];
}



#pragma mark - Textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
