//
//  TAADownloadViewController.m
//  RTVNH
//
//  Created by Andy Pijpers on 02-06-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import "TAADownloadViewController.h"

@interface TAADownloadViewController ()

@end

@implementation TAADownloadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] openURL:
     [NSURL URLWithString:@"https://itunes.apple.com/nl/app/rtv-nh/id421385561?mt=8&uo=4"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
