//
//  TAAVraag.m
//  RTVNH
//
//  Created by student on 23-05-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import "TAAVraag.h"
#import "TAAScramble.h"

@implementation TAAVraag
@synthesize gehusseldWoord = _gehusseldWoord;


- (NSString *) gehusseldWoord
{
    if (!_gehusseldWoord) {
        _gehusseldWoord = [TAAScramble scrambleString:self.randomWoord];
    }
    return _gehusseldWoord;
}

- (NSString *) gecensureerdeKop
{
    NSMutableString *gecensureerdWoord = [[NSMutableString alloc] init];
    for (int i = 0 ; i < [self.randomWoord length] ; i++) {
        [gecensureerdWoord appendString:@"_"];
    }
    
    NSString *gecensureerdeKop = [self.krantenKop stringByReplacingOccurrencesOfString:self.randomWoord withString:gecensureerdWoord];

    return gecensureerdeKop;
}


- (NSString *) randomWoordLowercase
{
    return [self.randomWoord lowercaseString];
}


@end
