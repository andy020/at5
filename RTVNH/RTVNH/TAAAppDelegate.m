//
//  TAAAppDelegate.m
//  RTVNH
//
//  Created by student on 23-05-14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "TAAAppDelegate.h"
#import <Parse/Parse.h>
#import "GAI.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"




@implementation TAAAppDelegate



@synthesize window;

//- (BOOL)isAppClean
//{
//    BOOL isClean = NO;
//    NSLog(@"dit is de cleantijd: %hhd", isClean);
//    
//    NSDate *now = [NSDate date];
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *components = [gregorian components:(NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSTimeZoneCalendarUnit) fromDate:now];
//    [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    
//    NSInteger day = [components day]; // Sunday == 1
//    if (day == 1) {
//        NSInteger hour = [components hour];
//        //NSInteger minute = [components minute]; -- not used but here's how to access it
//        if (hour == 00 || hour == 01) {// covers 8:00 - 9:59 PM
//            isClean = YES;
//        }
//    }
//    return isClean;
//}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:
(NSDictionary *)launchOptions {


    NSString *bundleIdentifier = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    NSLog(@"test: %@",bundleIdentifier);
    
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
        [Parse setApplicationId:@"VJRB2NpuTHY0Zz4L39EcSZYwvWgy2Nfjz3IZKwMO"
                      clientKey:@"s59PCytzpyKMd3EHkfndBeXTLylNi8zf1YfmY9Rx"];
        [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
        [GAI sharedInstance].dispatchInterval = 20;
        // Optional: set Logger to VERBOSE for debug information.
        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        // Initialize tracker. Replace with your tracking ID.
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-8308417-11"];

    } else if  ([appType isEqualToString:@"AT5"]) {
        [Parse setApplicationId:@"AZos2HC27upLo4PgF9APUvPr5xMRfbtFnKRA2hYW"
                      clientKey:@"9zZkTu3sNXXkqJZslsGGGcLrHQWfcmRkcKtiZB6Z"];
        [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
        [GAI sharedInstance].dispatchInterval = 20;
        // Optional: set Logger to VERBOSE for debug information.
        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        // Initialize tracker. Replace with your tracking ID.
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-3514065-31"];
    } else {
        NSLog(@"it is an unknown app");
    }
    
    if (![self connected])
    {
        // not connected
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Geen Netwerk Beschikbaar" message:@"Controleer uw netwerkinstellingen!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }else{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"Internet Connectie Correct");

    }

    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 480)
    {
        // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone35
        UIStoryboard *iPhone35Storyboard = [UIStoryboard storyboardWithName:@"4Iphone" bundle:nil];
        
        // Instantiate the initial view controller object from the storyboard
        UIViewController *initialViewController = [iPhone35Storyboard instantiateInitialViewController];
        
        // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        
        // Set the initial view controller to be the root view controller of the window object
        self.window.rootViewController  = initialViewController;
        
        // Set the window object to be the key window and show it
        [self.window makeKeyAndVisible];
    }
    
    if (iOSDeviceScreenSize.height == 568)
    {   // iPhone 5 and iPod Touch 5th generation: 4 inch screen
        // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
        UIStoryboard *iPhone4Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *initialViewController = [iPhone4Storyboard instantiateInitialViewController];
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController  = initialViewController;
        [self.window makeKeyAndVisible];

        
    }
    // Override point for customization after application launch.
    return YES;
}


- (void)dealloc {


}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
