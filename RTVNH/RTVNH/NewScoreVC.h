//
//  NewScoreVC.h
//  Kwisthet
//
//  Created by student on 27-08-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewScoreVC : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *Save;
- (IBAction)editingChanged;
@end
