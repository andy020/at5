//
//  TAAViewController.h
//  RTVNH
//
//  Created by student on 23-05-14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAAVraag.h"



@interface TAAVraagViewController : UIViewController <UITextFieldDelegate>{
    IBOutlet UIImageView *vraagImage;
    IBOutlet UIImageView *background;
    IBOutlet UILabel *timerLabel;


    NSInteger count;
    float seconds;
    NSTimer *timer;

}

@property (strong, nonatomic) IBOutlet UIProgressView *roundProgressLarge;
@property (nonatomic, strong) TAAVraag *vraag;
@property (nonatomic, strong) TAAVraag *zin;
@property (nonatomic) int score;
@property (nonatomic) int highScore;
@end