//
//  TAAGoedViewController.h
//  RTVNH
//
//  Created by student on 29-05-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAAVraag.h"

@interface TAAGoedViewController : UIViewController{
IBOutlet UIImageView *background;
IBOutlet UIImageView *alert_correct;
IBOutlet UIButton *volgendeGoed;

}
@property (nonatomic, strong) TAAVraag *vraag;


@end
