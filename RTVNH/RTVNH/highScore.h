//
//  highScore.h
//  Kwisthet
//
//  Created by student on 27-08-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface highScore : NSObject
@property (nonatomic, strong) NSString *naam;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) UILabel *score;
@end
