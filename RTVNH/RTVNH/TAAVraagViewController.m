//
//  TAAViewController.m
//  RTVNH
//
//  Created by student on 23-05-14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "TAAVraagViewController.h"
#import "TAAStore.h"
#import "timeProgressView.h"
#import "JMImageCache.h"

const int aantalVragen = 3;


@interface TAAVraagViewController ()

@property (nonatomic, weak)IBOutlet UILabel *krantenKop;
@property (nonatomic)IBOutlet UILabel *gehusseldWoord;
@property (weak, nonatomic) IBOutlet UITextField *antwoordField;
@property (weak, nonatomic) IBOutlet UILabel *vraagNummer;
@property (nonatomic, strong) UIProgressView *progressView;

@end
@implementation TAAVraagViewController


@synthesize roundProgressLarge;



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setupQuiz];
    count++;
    NSString *nummer = [NSString stringWithFormat:@"Vraag %ld van %d",(long)count,aantalVragen];
    self.vraagNummer.text = nummer;
    self.roundProgressLarge.progress = (float)seconds/25.0f;


    [TAAStore quizStore];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [TAAStore quizStore];
    TAAVraag *vraag = self.vraag;
    vraag.antWoord = self.antwoordField.text;
    self.antwoordField.text = @"";
    self.antwoordField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
   
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    count = 0;
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
        NSString* const backGround = @"http://www.rtvnh.nl/static/img/kwisthet/background.png";
        NSString* const kader = @"http://www.rtvnh.nl/static/img/kwisthet/vraag_nummer_kader.png";
        [background setImageWithURL:[NSURL URLWithString:backGround]
                        placeholder:[UIImage imageNamed:@"background.png"]];
        [vraagImage setImageWithURL:[NSURL URLWithString:kader]
                        placeholder:[UIImage imageNamed:@"vraag_nummer_kader.png"]];
    UIColor *tintColor = [UIColor colorWithRed:0.03f green:0.64f blue:0.83f alpha:1.00f];
    [[timeProgressView appearance] setTintColor:tintColor];
    self.roundProgressLarge.progress = 0.7;
        
    } else if  ([appType isEqualToString:@"AT5"]) {
        NSString* const backGround = @"http://www.rtvnh.nl/static/img/kwisthet/AT5_background.png";
        NSString* const kader = @"http://www.rtvnh.nl/static/img/kwisthet/vraag_nummer_kader.png";
        [background setImageWithURL:[NSURL URLWithString:backGround]
                        placeholder:[UIImage imageNamed:@"background.png"]];
        [vraagImage setImageWithURL:[NSURL URLWithString:kader]
                        placeholder:[UIImage imageNamed:@"vraag_nummer_kader.png"]];
    UIColor *tintColor = [UIColor colorWithRed:1.00f green:1.00f blue:1.00f alpha:1.00f];
    [[timeProgressView appearance] setTintColor:tintColor];
    self.roundProgressLarge.progress = 0.7;
    } else {
        NSLog(@"it is an unknown app");
    }

}

	// Do any additional setup after loading the view, typically from a nib.


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)setupQuiz
{
    int long aantal = [[TAAStore quizStore].allItems count];
//    if ([[TAAStore quizStore].allItems count]<1) {
//                    [self performSegueWithIdentifier:@"eindeQuiz" sender:self];
//    }
    
    NSArray *allItems = [TAAStore quizStore].allItems;
    
    int index = arc4random() %aantal;
    self.vraag = allItems[index];
    
    self.krantenKop.text = [self.vraag gecensureerdeKop];
    self.gehusseldWoord.text = self.vraag.gehusseldWoord;
    [self.gehusseldWoord.text lowercaseString];
    
    
    [[TAAStore quizStore] voegVraagToeAanGevraagdeVragen:self.vraag];
    [[TAAStore quizStore] removeVraag:self.vraag];
    
 
    // 1
    seconds = 25.0;
    // 2
    timerLabel.text = [NSString stringWithFormat:@"%li", (long)seconds];
    self.roundProgressLarge.progress = seconds/25.0f;
    // 3
    timer = [NSTimer scheduledTimerWithTimeInterval:0.10f
                                             target:self
                                           selector:@selector(subtractTime)
                                           userInfo:nil
                                            repeats:YES];
    
    [self.antwoordField becomeFirstResponder];

}

- (void)subtractTime {
    // 1
    seconds = seconds - 0.100;
    timerLabel.text = [NSString stringWithFormat:@"%.0f",seconds];
    self.roundProgressLarge.progress = (float)seconds/25.0f;
    
    // 2
    if (seconds < 0.1) {
        
    
        if (count == aantalVragen) {
            
            [self performSegueWithIdentifier:@"eindFoutAntwoord" sender:self];

        } else {
            
            [self performSegueWithIdentifier:@"foutAntwoord" sender:self];
        }
    
        [timer invalidate];
    }
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id vc = segue.destinationViewController;
    [vc setVraag:self.vraag];
}

- (IBAction)unwindToThisViewController:(UIStoryboardSegue *)unwindSegue
{
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.vraag.antWoord = textField.text;
    textField.text = @"";
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [textField resignFirstResponder];




    [timer invalidate];
    

    BOOL testBool = [[self.vraag.antWoord lowercaseString] isEqualToString:[self.vraag.randomWoord lowercaseString]];
    
    if (count == aantalVragen) {
        if (testBool) {
            [self performSegueWithIdentifier:@"eindGoedAntwoord" sender:self];
        } else {
            [self performSegueWithIdentifier:@"eindFoutAntwoord" sender:self];
        }
    } else {
        if (testBool) {
            [self performSegueWithIdentifier:@"goedAntwoord" sender:self];
        } else {
            [self performSegueWithIdentifier:@"foutAntwoord" sender:self];
        }
    }
    
    self.vraag.isAntwoordGoed = testBool;
    
    if (testBool) {
        self.vraag.vraagScore = seconds * 10.0;
//        NSLog(@"dit is het aantal seconden dat wordt opgeslagen: %f", self.vraag.vraagScore);

    }

    return YES;
}

@end