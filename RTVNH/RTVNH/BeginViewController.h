//
//  BeginViewController.h
//  Kwisthet
//
//  Created by student on 31-07-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface BeginViewController : GAITrackedViewController{
IBOutlet UIImageView *background;
IBOutlet UIButton *begin;
}

@property (weak, nonatomic) IBOutlet UILabel *uitlegLabel;
@end
