//
//  TAAScramble.h
//  RTVNH
//
//  Created by Andy Pijpers on 26-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAAScramble : NSObject

+ (NSString *)scrambleString:(NSString *)toScramble;

@end
