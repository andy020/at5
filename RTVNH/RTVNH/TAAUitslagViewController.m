//
//  TAAUitslagViewController.m
//  RTVNH
//
//  Created by Andy Pijpers on 31-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#define facebookView 1
#define twitterView 2
#import "TAAUitslagViewController.h"
#import "TAAVraagViewController.h"
#import "TAAStore.h"
#import "TAAVraag.h"
#import "TAAShare.h"
#import <Parse/Parse.h>
#import "JMImageCache.h"

@class TAAVraagViewController;
@interface TAAUitslagViewController ()
@end


@implementation TAAUitslagViewController
int long score = 0;
int long highScore;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)saveScore {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:score forKey:@"highScore"];
    [defaults synchronize];
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    quizCount++;
    
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
        NSString* backGround = @"http://www.rtvnh.nl/static/img/kwisthet/background.png";
        [background setImageWithURL:[NSURL URLWithString:backGround]
                        placeholder:[UIImage imageNamed:@"background.png"]];
            NSLog(@"Dit is wat hij cached: %@", background);
        self.textLabel.text = [NSString stringWithFormat:@"Altijd op de hoogte blijven?   Download de RTV NH app!"];
        NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/NH_app_icon.png"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *NH_app_icon = [UIImage imageWithData:data];
        [download setImage:NH_app_icon forState:UIControlStateNormal]; // Hier word de image van de Volgende Button
        
        NSURL *url2 = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/btn_speel_opnieuw.png"];
        NSData *data2 = [NSData dataWithContentsOfURL:url2];
        UIImage *btn_speel_opnieuw = [UIImage imageWithData:data2];
        [speelopnieuw setImage:btn_speel_opnieuw forState:UIControlStateNormal]; // Hier word de image van de Volgende Button Opgeroepen.

    NSURL *url3 = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/btn_share.png"];
    NSData *data3 = [NSData dataWithContentsOfURL:url3];
    UIImage *btn_volgende = [UIImage imageWithData:data3];
    [share setImage:btn_volgende forState:UIControlStateNormal]; // Hier word de image van de Volgende Button Opgeroepen.
        
} else if  ([appType isEqualToString:@"AT5"]) {
    NSString* const backGround = @"http://www.rtvnh.nl/static/img/kwisthet/AT5_background.png";
    [background setImageWithURL:[NSURL URLWithString:backGround]
                    placeholder:[UIImage imageNamed:@"AT5_background.png"]];
    self.textLabel.text = [NSString stringWithFormat:@"Altijd op de hoogte blijven?   Download de AT5 app!"];
    NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_app_icon.png"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *NH_app_icon = [UIImage imageWithData:data];
    [download setImage:NH_app_icon forState:UIControlStateNormal]; // Hier word de image van de Download Button Opgeroepen.

        NSURL *url2 = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_btn_speel_opnieuw.png"];
        NSData *data2 = [NSData dataWithContentsOfURL:url2];
        UIImage *btn_speel_opnieuw = [UIImage imageWithData:data2];
        [speelopnieuw setImage:btn_speel_opnieuw forState:UIControlStateNormal]; // Hier word de image van de Speel Opnieuw Button Opgeroepen.

    NSURL *url3 = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_btn_share.png"];
    NSData *data3 = [NSData dataWithContentsOfURL:url3];
    UIImage *btn_volgende = [UIImage imageWithData:data3];
    [share setImage:btn_volgende forState:UIControlStateNormal]; // Hier word de image van de Share Button Opgeroepen.
    
    
    
}



    NSArray *allItems = [TAAStore quizStore].alleGevraagdeVragen;

    
    int long aantalItems = [allItems count];
    
    if (aantalItems==18) {
        speelopnieuw.enabled = NO;
        speelopnieuw.backgroundColor = [UIColor grayColor];
        NSString *eindMessage =[[NSString alloc] initWithFormat:@"Kom morgen terug om Kwisthet weer te spelen en je High Score te verbeteren."];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Goed Gedaan!" message:(NSString *)eindMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
        [alert show];
    }
    
    int tellertje = 0;


    score = 0;
        
    for (TAAVraag *item in allItems) {
        
         NSLog(@"tellertje %d",tellertje);
        if (tellertje >= aantalItems - 3) {
            
            score = score + lroundf(item.vraagScore);
            NSLog(@"nieuwe score: %ld",score);
        }
        
        tellertje++;
    }
        
    self.scoreLabel.text = [NSString stringWithFormat:@"%ld punten",score];



    highScore =[[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
    
    
    if (score > highScore) {
        
        highScore = score;
        
        self.highScoreLabel.text = [NSString stringWithFormat:@"%ld punten",highScore];
        

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:highScore forKey:@"highScore"];
        [defaults synchronize];
        

        
        NSString *scoreMessage =[[NSString alloc] initWithFormat:@"High score! Goed Gedaan! Je hebt de hoogste score tot dusver behaald: %li punten",highScore];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"High Score!" message:(NSString *)scoreMessage delegate:self cancelButtonTitle:@"OK"
            otherButtonTitles:@"HighScore",nil];
        
        [alert show];

        
    } else {
        
        self.highScoreLabel.text = [NSString stringWithFormat:@"%ld punten",highScore];
        
        score = 0;
    
    
    [self.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    [self setModalPresentationStyle:UIModalPresentationCurrentContext];
        

       }


    }

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:@"HighScore"]) {
        
        [self performSegueWithIdentifier:@"newScore" sender:nil];
        
    }
}

+ (void) saveHighScore:(NSNumber *)score withName:(NSString *)name {
    PFObject *scoreObject = [[PFObject alloc] initWithClassName:@"HighScore"];
    [scoreObject setObject:score forKey:@"score"];
    [scoreObject save];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)downButton:(id)sender
{
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
    [[UIApplication sharedApplication] openURL:
     [NSURL URLWithString:@"https://itunes.apple.com/nl/app/rtv-nh/id421385561?mt=8&uo=4"]];
        
    } else if  ([appType isEqualToString:@"AT5"]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:@"https://itunes.apple.com/nl/app/at5/id585591468?mt=8&uo=4"]];
    } else {
        NSLog(@"it is an unknown app");
    }

}

int quizCount = 0;
- (IBAction)speelOpnieuwButton:(id)sender
{
    [self performSegueWithIdentifier:@"speelOpniew" sender:self];

}



-(IBAction)Screenshot {
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *imageView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *imageData = UIImagePNGRepresentation(imageView); //you can use PNG too
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"schreenshotimage.png"];
    [imageData writeToFile:documentsDirectory atomically:YES];

    
//    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:data], nil, nil, nil);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
