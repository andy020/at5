//
//  TAAVraag.h
//  RTVNH
//
//  Created by student on 23-05-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAAVraag : NSObject

@property (nonatomic) int vraagNummer;
@property (nonatomic) int vraagDuur;
@property (nonatomic) float vraagScore;

@property (nonatomic, strong) NSString *krantenKop;
@property (nonatomic, strong) NSString *randomWoord;
@property (nonatomic, strong) NSString *gehusseldWoord;
@property (nonatomic, strong) NSString *antWoord;

@property (nonatomic, assign) BOOL isVraagGevraagd;
@property (nonatomic, assign) BOOL isAntwoordGoed;


- (NSString *) gecensureerdeKop;
- (NSString *) randomWoordLowercase;

@end
