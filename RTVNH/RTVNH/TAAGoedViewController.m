//
//  TAAGoedViewController.m
//  RTVNH
//
//  Created by student on 29-05-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import "TAAGoedViewController.h"
#import "TAAStore.h"
#import "TAAVraag.h"
#import "JMImageCache.h"

@interface TAAGoedViewController ()


@end

@implementation TAAGoedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
    background.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                  [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/background.png"]]];
    alert_correct.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                              [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/alert_correct.png"]]];
    NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/btn_volgende.png"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *btn_volgende = [UIImage imageWithData:data];
    [volgendeGoed setImage:btn_volgende forState:UIControlStateNormal]; // Hier word de image van de Volgende Button Opgeroepen.
#warning Hier moeten de plaatjes van AT5        
    } else if  ([appType isEqualToString:@"AT5"]) {
        background.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                   [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_background.png"]]];
        alert_correct.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                      [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_alert_correct.png"]]];
        NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_btn_volgende.png"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *btn_volgende = [UIImage imageWithData:data];
        [volgendeGoed setImage:btn_volgende forState:UIControlStateNormal]; // Hier word de image van de Volgende Button Opgeroepen.
    } else {
        NSLog(@"it is an unknown app");
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
