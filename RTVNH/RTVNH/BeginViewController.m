//
//  BeginViewController.m
//  Kwisthet
//
//  Created by student on 31-07-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import "BeginViewController.h"



@interface BeginViewController ()
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation BeginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
       [[NSUserDefaults standardUserDefaults] boolForKey:@"SettingsShowTutorialOnLaunch"];
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
         NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
        self.screenName = @"RTV NH";
        background.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                   [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/background.png"]]];
        NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/btn_begin.png"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *btn_begin = [UIImage imageWithData:data];
        [begin setImage:btn_begin forState:UIControlStateNormal];
//        self.uitlegLabel.text = [NSString stringWithFormat:@"Vergeet niet onze RTVNH app te downloaden om op de hoogte te blijven van ons nieuws!."];
    } else if  ([appType isEqualToString:@"AT5"]) {
        background.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                   [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_background.png"]]];
        NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_btn_begin.png"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *btn_begin = [UIImage imageWithData:data];
        [begin setImage:btn_begin forState:UIControlStateNormal];
//        self.uitlegLabel.text = [NSString stringWithFormat:@"Vergeet niet onze AT5 app te downloaden om op de hoogte te blijven van ons nieuws!."];
    } else {
//        NSLog(@"it is an unknown app");
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

   @end
