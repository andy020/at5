//
//  XMLData.h
//  XMLParse
//
//  Created by Stijn Oomes on 8/7/14.
//  Copyright (c) 2014 Stijn Oomes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLData : NSObject <NSXMLParserDelegate>
{
    NSMutableString *myWord;
    NSString *tag;

}

@property (nonatomic) NSMutableArray *myArray;


-(BOOL)parseDocumentWithURL:(NSURL *)url;

@end
