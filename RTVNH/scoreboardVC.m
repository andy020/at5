//
//  scoreboardVC.m
//  Kwisthet
//
//  Created by student on 27-08-14.
//  Copyright (c) 2014 student. All rights reserved.
//

#import "scoreboardVC.h"
#import "highScore.h"


@interface scoreboardVC ()

@end

@implementation scoreboardVC{
}

- (id)initWithCoder:(NSCoder *)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"HighScore";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"naam";
        
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = NO;
        
        // The number of objects to show per page
        self.objectsPerPage = 10;
        


    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    if ([appType isEqualToString:@"RTVNH"]) {
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"background.png"]];
    [self.tableView setBackgroundView:imageView];
    }else if  ([appType isEqualToString:@"AT5"]) {
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"backgroundAT5.png"]];
        [self.tableView setBackgroundView:imageView];
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTable:)
                                                 name:@"refreshTable"
                                               object:nil];
}

- (void)refreshTable:(NSNotification *) notification
{
   [self loadObjects];// Reload the recipes
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshTable" object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (PFQuery *)queryForTable
{
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
            [query orderByDescending:@"score"];
    query.limit = 10;

    
    // and then subsequently do a query against the network.
    if ([self.objects count] == 0) {

     query.cachePolicy = kPFCachePolicyCacheThenNetwork;
            
        [self.tableView reloadData];
//        [query orderByDescending:@"score"];
        [self.refreshControl endRefreshing];
        
    
    }
        return query;
    
}


// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the first key in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object
{
    static NSString *simpleTableIdentifier = @"ScoreCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE, MMMM d yyyy"];
        NSDate *date = [object createdAt];
        [dateFormatter stringFromDate:date];
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    UILabel *nameLabel = (UILabel*) [cell viewWithTag:101];
    nameLabel.text = [object objectForKey:@"naam"];
    
    
    UILabel *scoreLabel = (UILabel*) [cell viewWithTag:103];
    scoreLabel.text = [object objectForKey:@"score"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{



}


- (void) objectsDidLoad:(NSError *)error
{
    [super objectsDidLoad:error];

    
    NSLog(@"error: %@", [error localizedDescription]);
}

@end