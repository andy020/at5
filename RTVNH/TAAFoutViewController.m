//
//  TAAFoutViewController.m
//  RTVNH
//
//  Created by Andy Pijpers on 29-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import "TAAFoutViewController.h"
#import "TAAStore.h"
#import "TAAVraag.h"
#import "JMImageCache.h"

@interface TAAFoutViewController ()
@property (nonatomic)IBOutlet UILabel *randomWoord;

@end

@implementation TAAFoutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *appType = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppType"];
    
    if ([appType isEqualToString:@"RTVNH"]) {
    background.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                               [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/background.png"]]];
    alert_fout.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                  [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/alert_fout.png"]]]; // Hier word de image van de Fout Image Opgeroepen.
    NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/btn_volgende.png"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *btn_volgende = [UIImage imageWithData:data];
    [volgende setImage:btn_volgende forState:UIControlStateNormal]; // Hier word de image van de Volgende Button Opgeroepen.
        
    } else if  ([appType isEqualToString:@"AT5"]) {
        //antwoordLabel.textColor = [UIColor whiteColor];
        textLabel.textColor = [UIColor whiteColor];
        background.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                   [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_background.png"]]];
        alert_fout.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                   [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/At5_alert_fout.png"]]]; // Hier word de image van de Fout Image Opgeroepen.
        NSURL *url = [NSURL URLWithString:@"http://www.rtvnh.nl/static/img/kwisthet/AT5_btn_volgende.png"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *btn_volgende = [UIImage imageWithData:data];
        [volgende setImage:btn_volgende forState:UIControlStateNormal]; // Hier word de image van de Volgende Button Opgeroepen.
    } else {
        NSLog(@"it is an unknown app");
    }


    self.randomWoord.text = self.vraag.randomWoordLowercase;    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
