//
//  TAAFoutViewController.h
//  RTVNH
//
//  Created by Andy Pijpers on 29-05-14.
//  Copyright (c) 2014 Andy Pijpers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAAVraag.h"

@interface TAAFoutViewController : UIViewController{
    IBOutlet UIImageView *background;
    IBOutlet UIImageView *alert_fout;
    IBOutlet UIButton *volgende;
    IBOutlet UILabel *textLabel;
    //IBOutlet UILabel *antwoordLabel;
}
@property (nonatomic, strong) TAAVraag *vraag;
@end
